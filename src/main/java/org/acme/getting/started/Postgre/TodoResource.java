package org.acme.getting.started.Postgre;

import io.quarkus.panache.common.Sort;

import javax.transaction.Transactional;
import javax.validation.Valid;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;

@Path("/api")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class TodoResource {
    @GET
    public List<Todo> getAll() {
        return Todo.listAll(Sort.by("order"));
    }

    @GET
    @Path("/search/{word}/{pageIndex}")
    public List<Todo> search(@PathParam("word") String word, @PathParam("pageIndex") int pageIndex) {
        //return word + pageIndex;
        return Todo.search(word, pageIndex);
    }

    @POST
    @Transactional
    public Response create(@Valid Todo todo) {
        todo.persist();
        return Response.status(Response.Status.CREATED).entity(todo).build();
    }

    @PATCH
    @Path("/{id}")
    @Transactional
    public Todo update(@Valid Todo todo, @PathParam("id") Long id) {
        Todo entity = Todo.findById(id);
        entity.id = todo.id;
        entity.title = todo.title;
        entity.url = todo.url;
        entity.completed = todo.completed;
        entity.order = todo.order;
        return entity;
    }

    @DELETE
    @Transactional
    public Response deleteCompleted() {
        Todo.deleteCompleted();
        return Response.noContent().build();
    }
}
