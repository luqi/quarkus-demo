package org.acme.getting.started.Postgre;

import io.quarkus.hibernate.orm.panache.PanacheEntity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.validation.constraints.NotBlank;
import java.util.List;

@Entity
public class Todo extends PanacheEntity {
    @Column(unique = true)
    @NotBlank
    public String title;
    public String url;
    public boolean completed;
    public int order;

    public static void deleteCompleted() {
        delete("completed", true);
    }

    public static List<Todo> search(String word, int pageIndex) {
        return find("title like ?1 and completed ?2", word, false).page(pageIndex, 3).list();
    }
}
