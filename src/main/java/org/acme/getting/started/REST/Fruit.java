package org.acme.getting.started.REST;

import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

public class Fruit {
    @NotBlank(message = "Title may not be blank")
    public String name;

    @NotBlank(message = "Description may not be blank")
    @Size(min = 10, message = "More description needed")
    public String description;

    public Fruit() {
    }

    public Fruit(String name, String description) {
        this.name = name;
        this.description = description;
    }
}
